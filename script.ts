import 'readline';
import * as readline from 'node:readline/promises'
import {stdin, stdout} from 'node:process'

function get_env_name(value: string, pos: number)
{
    let end: number = value.indexOf(" ", pos)
    if (end === -1)
        end = value.length;
    console.log(end, pos)
    const env_name = value.substring(pos + 1, end)
    const env_value = process.env[env_name];
    if (!env_value)
    {
       return value.replace("$".concat(env_name), "")
    }
    return value.replace("$".concat(env_name), env_value)
}

function parse_command(cmd: string)
{
    let pos: number = cmd.indexOf("$")
    cmd = get_env_name(cmd, pos)
    console.log(cmd)
}
async function main() {
    const rl = readline.createInterface(stdin, stdout);
    let str: string
    let finish: boolean = false;
    while (!finish) {
        rl.on("SIGINT", () => rl.resume())
        str = await rl.question("please enter something: ")
        if (str === "exit")
            break
        parse_command(str)
    }
    rl.close()
}

if (require.main === module)
{
    main()
}